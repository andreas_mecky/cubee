#pragma once
#include "Bucket.h"
#include <sprites\Sprite.h>

class MovingCells : public ds::GameObject {

typedef std::vector<ds::Sprite> Cells;

public:
	MovingCells() {}
	~MovingCells(void) {}
	void setGrid(ColorGrid* grid) {
		m_Grid = grid;
	}
	void add(int firstX,int firstY,int secondX,int secondY);
	void addRefill(int xpos,int color);
	void update(float elapsed);
	void render();
	const int numActive() const {
		return m_Cells.size();
	}
	const bool isMoving() const {
		return m_Moving;
	}
private:
	ColorGrid* m_Grid;
	Cells m_Cells;
	bool m_Moving;
};

