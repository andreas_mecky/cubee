#include "Bucket.h"
#include "MovingCells.h"
#include <math\GameMath.h>
#include <utils\Log.h>

// -------------------------------------------------------
// Grid
// -------------------------------------------------------
Bucket::Bucket(void) : m_Grid(GRID_SX,GRID_SY) {		
	m_MovingCells = new MovingCells();
	
	m_MovingCells->setGrid(&m_Grid);
	clear();
}

Bucket::~Bucket() {
	delete m_MovingCells;
}

// -------------------------------------------------------
// Init
// -------------------------------------------------------
void Bucket::init() {
	
	for ( int i = 0; i < GRID_SX; ++i ) {
		ds::Sprite* refillCell = &m_Refill[i];
		refillCell->setPosition(Vector2f(START_X+i*CELL_SIZE,REFILL_Y_POS));
		refillCell->setTextureRect(ds::Rect(BLOCK_TOP,0,CELL_SIZE,CELL_SIZE));
		refillCell->setColor(ds::Color(255,255,255,64));		
	}
	
	int i = 0;
	for ( int x = 0; x < GRID_SX; ++x ) {	
		for ( int y = 0; y < GRID_SY; ++y ) {			
			ds::Sprite* sp = &m_BackGrid[i];
			sp->setPosition(Vector2f(START_X + x * CELL_SIZE,START_Y + y * CELL_SIZE));
			sp->setTextureRect(ds::Rect(0,0,CELL_SIZE,CELL_SIZE));
			++i;
		}
	}
	m_BottomBar.setPosition(Vector2f(512,102));
	m_BottomBar.setTextureRect(ds::Rect(190,0,400,10));	
	m_TopBar.setPosition(Vector2f(512,START_Y + GRID_SY * CELL_SIZE - 15));
	m_TopBar.setTextureRect(ds::Rect(190,0,400,10));	

	m_FirstSelection.setPosition(Vector2f(START_X,START_Y));
	m_FirstSelection.setTextureRect(SELECTION_RECT);
	m_FirstSelection.setActive(false);

	m_MovingCells->setRenderer(m_Renderer);
}
// -------------------------------------------------------
// Clear grid
// -------------------------------------------------------
void Bucket::clear() {
	for ( int x = 0; x < GRID_SX; ++x ) {
		m_Refill[x].setID(-1);		
	}
	m_Grid.clear();
	m_FirstSelection.setActive(false);
	m_Mode = BK_RUNNING;
	m_Filled = 0;
}

// -------------------------------------------------------
// Fill entire grid
// -------------------------------------------------------
void Bucket::fill(int minCol,int maxCol) {
	for ( int x = 0; x < GRID_SX; ++x ) {
		int pieces = ds::math::random(minCol,maxCol);
		fillRow(x,pieces);
	}
	calculateFillRate();
}

// -------------------------------------------------------
// Fill row
// -------------------------------------------------------
void Bucket::fillRow(int row,int pieces) {
	for ( int y = 0; y < pieces; ++y ) {
		ds::Sprite c;
		c.setID(ds::math::random(0,MAX_COLORS-1));
		c.setActive(true);
		int offset = c.getID() * CELL_SIZE;
		c.setTextureRect(ds::Rect(BLOCK_TOP,offset,CELL_SIZE,CELL_SIZE));
		c.setPosition(Vector2f(START_X + row * CELL_SIZE,START_Y + y * CELL_SIZE));
		m_Grid.set(row,y,c);
	}
}

// -------------------------------------------------------
// Is row full
// -------------------------------------------------------
bool Bucket::isRowFull(int row) {
	int cnt = 0;
	for ( int y = 0; y < GRID_SY; ++y ) {
		if ( m_Grid.isFree(row,y) ) {
			++cnt;
		}
	}
	return cnt == 0;
}
// -------------------------------------------------------
// Move row upwards
// -------------------------------------------------------
void Bucket::moveRow(int row) {
	for ( int y = GRID_SY - 1; y > 0; --y ) {
		if ( !m_Grid.isFree(row,y-1) ) {
			m_Grid.set(row,y,m_Grid.get(row,y-1));
			m_Grid.remove(row,y-1);		
			// create moving cell
			m_MovingCells->add(row,y-1,row,y);	
		}
	}
}

// -------------------------------------------------------
// Refill
// -------------------------------------------------------
bool Bucket::refill(int pieces,bool move) {
	// move current refill nodes to nodes
	if ( move ) {
		for ( int i = 0; i < GRID_SX; ++i ) {
			ds::Sprite* node = &m_Refill[i];
			// if row is full we cannot refill and game is over
			if (isRowFull(i)) {
				LOG << "Row " << i << " is full";
				RowFullEvent evn;
				evn.row = i;
				getEvents().add(1,&evn,sizeof(RowFullEvent));
				return false;
			}
			moveRow(i);
			m_MovingCells->addRefill(i,node->getID());		
		}
	}
	/*
	// now build new refill line
	int tmp[GRID_SX];
	for ( int i = 0; i < GRID_SX; ++i ) {
		tmp[i] = -1;
	}
	for ( int i = 0; i < pieces; ++i) {
		tmp[i] = ds::math::random(0,MAX_COLORS-1);
	}	
	for ( int i = GRID_SX - 1; i > 0 ; --i ) {
		int idx = ds::math::random(0,GRID_SX-1);
		int current = tmp[i];
		tmp[i] = tmp[idx];
		tmp[idx] = current;
	}
	for ( int i = 0; i < GRID_SX; ++i ) {
		int current = tmp[i];
		if ( current != -1 ) {
			m_Refill[i].color = current;
		}
		else {
			m_Refill[i].color = -1;
		}
	}
	*/
	m_Mode = BK_REFILLING;
	for ( int i = 0; i < GRID_SX; ++i ) {
		ds::Sprite* refillCell = &m_Refill[i];
		refillCell->setID(ds::math::random(0,MAX_COLORS-1));
		int offset = refillCell->getID() * CELL_SIZE;
		refillCell->setTextureRect(ds::Rect(120,offset,CELL_SIZE,CELL_SIZE));
	}
	return true;
}

// -------------------------------------------------------
// Calculate fill rate
// -------------------------------------------------------
void Bucket::calculateFillRate() {
	m_Filled = 0;
	for ( int x = 0; x < GRID_SX; ++x ) {
		for ( int y = 0; y < GRID_SY; ++y ) {			
			if ( !m_Grid.isFree(x,y)) {
				++m_Filled;
			}
		}
	}
	LOG << "filled " << m_Filled;
	float percentage = static_cast<float>(m_Filled) / (static_cast<float>(GRID_SX) * static_cast<float>(GRID_SY)) * 100.0f;
	LOG << "percentage " << percentage;
	m_PercentFilled = static_cast<int>(percentage);
}
// -------------------------------------------------------
// Update
// -------------------------------------------------------
void Bucket::update(float elapsed) {
	if ( m_Mode == BK_REFILLING ) {
		m_MovingCells->update(elapsed);
		if ( m_MovingCells->numActive() == 0 ) {
			m_Mode = BK_RUNNING;	
			calculateFillRate();
		}
	}
	if ( m_Mode == BK_MOVING ) {
		m_MovingCells->update(elapsed);
		if ( m_MovingCells->numActive() == 0 ) {
			refill(GRID_SX);
			m_Mode = BK_REFILLING;
		}
	}	

	if ( m_Mode == BK_GLOWING ) {
		m_GlowTimer += elapsed;
		if ( m_GlowTimer > FLASH_TTL ) {
			m_Mode = BK_MOVING;
			m_Grid.remove(m_Points);
			m_RemovedCells.clear();
			m_Highlights.clear();
			m_Grid.dropCells(m_RemovedCells);
			for ( size_t i = 0; i < m_RemovedCells.size(); ++i ) {
				ds::DroppedCell* dc = &m_RemovedCells[i];
				m_MovingCells->add(dc->to.x,dc->to.y,dc->from.x,dc->from.y);
			}				
			m_Points.clear();			
		}
	}	
}

// -------------------------------------------------------
// Draw
// -------------------------------------------------------
void Bucket::render() {
	int offset = -1;
	for ( int x = 0; x < GRID_SX; ++x ) {
		m_Renderer->draw(m_Refill[x]);
	}
	for ( int x = 0; x < GRID_SX; ++x ) {
		for ( int y = 0; y < GRID_SY; ++y ) {			
			if ( !m_Grid.isFree(x,y)) {
				ds::Sprite& so = m_Grid(x,y);
				so.setPosition(Vector2f(START_X + x * CELL_SIZE,START_Y + y * CELL_SIZE));
				m_Renderer->draw(so);
			}
		}
	}

	m_MovingCells->render();

	m_Renderer->draw(m_FirstSelection);

	if ( m_Mode == BK_GLOWING ) {
		float a = sin(m_GlowTimer/FLASH_TTL) * 0.8f;
		for ( size_t i = 0; i < m_Highlights.size(); ++i ) {
			ds::Color* clr = m_Highlights[i].getColorPtr();
			clr->a = 1.0f - a;
			m_Highlights[i].setScale(Vector2f(1.0f + a*0.3f,1.0f +a*0.3f));
			m_Renderer->draw(m_Highlights[i]);
		}
	}
}

// -------------------------------------------------------
// Draw grid
// -------------------------------------------------------
void Bucket::drawGrid() {
	int total = GRID_SX * GRID_SY;
	for ( int x = 0; x < total; ++x ) {	
		m_Renderer->draw(m_BackGrid[x]);		
	}
	m_Renderer->draw(m_TopBar);
	m_Renderer->draw(m_BottomBar);
}

// -------------------------------------------------------
// is valid position
// -------------------------------------------------------
const bool Bucket::isValid(int x,int y) const {
	if ( x < 0 || x >= GRID_SX ) {
		return false;
	}
	if ( y < 0 || y >= GRID_SY ) {
		return false;
	}
	return true;
}

// -------------------------------------------------------
// is used
// -------------------------------------------------------
const bool Bucket::isUsed(int x,int y) const {
	return !m_Grid.isFree(x,y);	
}

// -------------------------------------------------------
// Swap cells
// -------------------------------------------------------
int Bucket::swapCells(int fx,int fy,int sx,int sy) {	
	ds::Sprite org = m_Grid(fx,fy);
	m_Grid.set(fx,fy,m_Grid(sx,sy));
	m_Grid.set(sx,sy,org);
	m_Highlights.clear();
	m_Points.clear();
	int total = findMatching(fx,fy);
	total += findMatching(sx,sy);	
	if ( total == 0 ) {		
		// not allowed - swap back
		org = m_Grid(fx,fy);
		m_Grid.set(fx,fy,m_Grid(sx,sy));
		m_Grid.set(sx,sy,org);
	}
	return total;
}

int Bucket::findMatching(int gx,int gy) {
	int total = 0;
	std::vector<ds::GridPoint> points;
	m_Grid.findMatchingNeighbours(gx,gy,points);
	if ( points.size() > 2 ) {
		total += points.size();
		for ( size_t i = 0; i < points.size(); ++i ) {
			ds::GridPoint* gp = &points[i];
			const ds::Sprite& c = m_Grid.get(gp->x,gp->y);
			ds::Sprite h;
			h.setTextureRect(ds::Rect(80,300,58,58));
			h.setPosition(Vector2f(START_X + gp->x * CELL_SIZE,START_Y + gp->y * CELL_SIZE));					
			h.setColor(COLOR_ARRAY[c.getID()]);
			m_Highlights.push_back(h);
			m_Points.push_back(points[i]);
		}
	}
	return total;
}

// -------------------------------------------------------
// Select cell
// -------------------------------------------------------
int Bucket::selectCell(const Vector2f& mousePos) {
	int ret = -1;
	if ( m_Mode == BK_RUNNING ) {		
		int x = (mousePos.x - START_X + 20) / CELL_SIZE;
		int my = 768 - mousePos.y;
		int y = (my - START_Y + 20) / CELL_SIZE;
		LOG << "picking at " << x << " " << y;
		if ( isValid(x,y) && isUsed(x,y)) {
			if ( !m_FirstSelection.isActive() ) {			
				m_FirstSelection.setPosition(Vector2f(START_X + x * CELL_SIZE,START_Y + y * CELL_SIZE));
				m_FirstSelection.setTarget(Vector2f(x,y));
				m_FirstSelection.setActive(true);
			}		
			else {
				ret = swapCells(x,y,m_FirstSelection.getTarget().x,m_FirstSelection.getTarget().y);
				if ( ret > 0 ) {
					m_GlowTimer = 0.0f;
					m_Mode = BK_GLOWING;
				}
				m_FirstSelection.setActive(false);
			}
		}
		else {
			m_FirstSelection.setActive(false);
		}
	}
	else {
		LOG << "not running";
	}
	return ret;
}