#include "MovingCells.h"

void MovingCells::add(int firstX,int firstY,int secondX,int secondY) {
	ds::Sprite c = m_Grid->get(secondX,secondY);
	c.setActive(false);
	m_Grid->set(secondX,secondY,c);
	ds::Sprite sp;
	sp.setPosition(Vector2f(START_X + firstX * CELL_SIZE,START_Y + firstY * CELL_SIZE));
	sp.setVelocity(Vector2f(0.0f,(START_Y + secondY * CELL_SIZE - sp.getPosition().y) /  MOVE_TTL));
	int offset = c.getID() * CELL_SIZE;
	sp.setTarget(Vector2f(secondX,secondY));
	sp.setTextureRect(ds::Rect(BLOCK_TOP,offset,CELL_SIZE,CELL_SIZE));
	m_Cells.push_back(sp);
}

void MovingCells::addRefill(int xpos,int color) {
	ds::Sprite c;
	c.setID(color);
	int offset = c.getID() * CELL_SIZE;
	c.setTextureRect(ds::Rect(BLOCK_TOP,offset,CELL_SIZE,CELL_SIZE));
	c.setActive(false);
	m_Grid->set(xpos,0,c);	
	ds::Sprite sp;
	sp.setPosition(Vector2f(START_X + xpos * CELL_SIZE,REFILL_Y_POS));
	sp.setVelocity(Vector2f(0.0f,(START_Y - REFILL_Y_POS) /  MOVE_TTL));
	sp.setTarget(Vector2f(xpos,0));
	sp.setTextureRect(ds::Rect(BLOCK_TOP,offset,CELL_SIZE,CELL_SIZE));
	m_Cells.push_back(sp);
}

void MovingCells::update(float elapsed) {
	m_Moving = !m_Cells.empty();
	Cells::iterator it = m_Cells.begin();
	while ( it != m_Cells.end()) {
		it->tick(elapsed);
		if ( it->getTimer() >= MOVE_TTL ) {
			Vector2f target = it->getTarget();
			ds::Sprite& c = m_Grid->get(target.x,target.y);
			c.setActive(true);
			m_Grid->set(target.x,target.y,c);
			it = m_Cells.erase(it);
		}
		else {
			it->move(elapsed);
			++it;
		}
	}
}

void MovingCells::render() {
	for ( size_t i = 0; i < m_Cells.size(); ++i) {
		m_Renderer->draw(m_Cells[i]);
	}
}