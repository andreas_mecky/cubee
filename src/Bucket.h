#pragma once
#include "Constants.h"
#include <lib\Grid.h>
#include <vector>
#include <base\GameObject.h>
#include <sprites\Sprite.h>

// -------------------------------------------------------
// ColorGrid
// -------------------------------------------------------
class ColorGrid : public ds::Grid<ds::Sprite> {

public:
	ColorGrid(int sizeX,int sizeY) : ds::Grid<ds::Sprite>(sizeX,sizeY) {}
	virtual ~ColorGrid() {}
	bool isMatch(const ds::Sprite& first,const ds::Sprite& right) {
		return first.getID() == right.getID();
	}
};

class MovingCells;

// -------------------------------------------------------
// Bucket
// -------------------------------------------------------
class Bucket : public ds::GameObject {

enum BucketMode {
	BK_RUNNING,
	BK_MOVING,
	BK_GLOWING,
	BK_REFILLING
};

typedef std::vector<ds::DroppedCell> RemovedCells;
typedef std::vector<ds::Sprite> Highlights;
typedef std::vector<ds::GridPoint> Points;

public:
	Bucket(void);
	~Bucket();
	void init();
	void clear();
	void update(float elapsed);
	void render();
	void drawGrid();
	void fillRow(int row,int pieces);
	void fill(int minCol,int maxCol);
	bool refill(int pieces,bool move = true);
	int selectCell(const Vector2f& mousePos);
	const bool isValid(int x,int y) const;
	const bool isUsed(int x,int y) const;
	int swapCells(int fx,int fy,int sx,int sy);
	void moveRow(int row);
	bool isRowFull(int row);
	const int getOccupied() const {
		return m_PercentFilled;
	}
private:	
	int findMatching(int gx,int gy);
	void calculateFillRate();
	Highlights m_Highlights;
	Points m_Points;
	ds::Sprite m_Refill[GRID_SX];
	RemovedCells m_RemovedCells;
	ds::Sprite m_FirstSelection;
	ColorGrid m_Grid;
	BucketMode m_Mode;
	MovingCells* m_MovingCells;
	bool m_Refilling;
	float m_GlowTimer;
	int m_Filled;
	int m_PercentFilled;
	ds::Sprite m_BackGrid[GRID_SX * GRID_SY];
	ds::Sprite m_TopBar;
	ds::Sprite m_BottomBar;
};


